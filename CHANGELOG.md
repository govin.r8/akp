## [1.1.1](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.1.0...v1.1.1) (2021-06-13)


### Bug Fixes

* change base image ([daad792](https://gitlab.com/ayedocloudsolutions/akp/commit/daad79261068a1d220ed52dba41de8c34e158bc3))

# [1.1.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.0.0...v1.1.0) (2021-06-13)


### Bug Fixes

* refactored configuration, renamed output_dir to release_dir ([d946917](https://gitlab.com/ayedocloudsolutions/akp/commit/d946917f7195908ca2907bf53cc1522f06f79bab))


### Features

* added Fission as app ([b22c433](https://gitlab.com/ayedocloudsolutions/akp/commit/b22c4333424b6191f6d8a6e99686303e0268563f))
* added Prometheus ServiceMonitors ([2557d0b](https://gitlab.com/ayedocloudsolutions/akp/commit/2557d0b690d088b81b61abc1e9f92d91edd8fcfa))
* added Sonarqube as app ([12aa1c7](https://gitlab.com/ayedocloudsolutions/akp/commit/12aa1c768346605c527e86f3806c9c16bc840af7))

# 1.0.0 (2021-05-27)


### Bug Fixes

* added main as release branch ([0aacedf](https://gitlab.com/ayedocloudsolutions/akp/commit/0aacedfcfd000639eda47ed13e7784b072228517))


### Features

* Launch ayedo Kubernetes Platform ([027eb4d](https://gitlab.com/ayedocloudsolutions/akp/commit/027eb4dd656fe2539b74bc62b10007ab52c87242))
